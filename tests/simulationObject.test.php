<?php
include('simulationObject.php');
include('connect.php');

class simulationObjectTest extends \PHPUnit_Framework_TestCase
{
  // Global variables for simulationObject tests
  protected $dbReservoir = array();
    
  // Set up
  public function setUp(){     
    $location_query = mysql_query("
      SELECT location_id, location_name
      FROM location_list
    ");
    $waterLevel = 100;
    while($loc_row = mysql_fetch_assoc($location_query))
    {
      $resevoir_query = mysql_query("
        SELECT ml.month_name, wl.dry_season
        FROM initial_water_level AS wl
        LEFT JOIN month_list AS ml
        ON wl.month_id = ml.month_id
        WHERE wl.location_id = ".$loc_row['location_id']."
      ");

      while($res_row = mysql_fetch_array($resevoir_query))
      {
        $tempArray[$res_row['month_name']] = $res_row['dry_season'];
      }
      $this->dbReservoir[$loc_row['location_name']] =  array(0 => $tempArray);

    }
    foreach(current($this->dbReservoir)[0] as $month => $level){
      $this->dbReservoir[key($this->dbReservoir)][0][$month] = $level*($waterLevel/100);
    }
  }
    
	/* Tests for getMonth:
		1: Test existing data
		2: Test non-existing reservoir
		3: Test non-existing year index
		4: Test non-existing month
	*/
	public function testGetMonth(){
    $expected = 33166;
    $test = new SimulationObject($this->dbReservoir);
    $this->assertEquals($test->getMonth("Cle Elum", 0, "January"), $expected);
	}
	
	public function testGetMonth2(){
    $expected = "Invalid reservoir!";
    $test = new SimulationObject($this->dbReservoir);
    $this->assertEquals($test->getMonth("Poseidon", 0, "January"), $expected);
	}
	
	public function testGetMonth3(){
    $expected = "Invalid year index!";
    $test = new SimulationObject($this->dbReservoir);
    $this->assertEquals($test->getMonth("Cle Elum", 1, "January"), $expected);
	}
	
	public function testGetMonth4(){
    $expected = "Invalid month name!";
    $test = new SimulationObject($this->dbReservoir);
    $this->assertEquals($test->getMonth("Cle Elum", 0, "Poseidon"), $expected);
	}
	
	/* Tests for getYear
		1: Test for existing data
		2: Test non-existing reservoir
		3: Test non-existing year
	*/
	public function testGetYear(){
    $expected = array(
      "January" => 33166, 
      "February" => 89273,
      "March" => 165172,
      "April" => 220979,
      "May" => 279532,
      "June" => 342205, 
      "July" => 301917,
      "August" => 181014,
      "September" => 95364,
      "October" => 23380,
      "November" => 11812,
      "December" => 37445);
    $test = new SimulationObject($this->dbReservoir);
    $this->assertEquals($test->getYear("Cle Elum", 0), $expected);
	}
	
	public function testGetYear2(){
    $expected = "Invalid reservoir!";
    $test = new SimulationObject($this->dbReservoir);
    $this->assertEquals($test->getYear("Poseidon", 0), $expected);
	}
	
	public function testGetYear3(){
    $expected = "Invalid year index!";
    $test = new SimulationObject($this->dbReservoir);
    $this->assertEquals($test->getYear("Cle Elum", 13), $expected);
	}
	
	/* Tests for getMonth
		1: Test for existing month
		2: Test for non-existing month
	*/
	public function testGetMonthName(){
    $expected = "January";
    $test = new Simulationobject($this->dbReservoir);
    $this->assertEquals($test->getMonthName(0), $expected);
	}
	
	public function testGetMonthName2(){
    $expected = "Invalid month index.";
    $test = new Simulationobject($this->dbReservoir);
    $this->assertEquals($test->getMonthName(13), $expected);
	}
	
	/* Tests for addYear
		1: Test that size of years is incremented when added to
		2: Test for invalid year array
		3: Test that year was correctly added
		4: Test for invalid water levels
	*/
	public function testAddYear(){
    $expected = 2;
    $testYear = array(
      "January" => 1,
      "February" => 1,
      "March" => 1,
      "April" => 1,
      "May" => 1,
      "June" => 1,
      "July" => 1,
      "August" => 1,
      "September" => 1,
      "October" => 1,
      "November" => 1,
      "December" => 1, 
    );
    $test = new Simulationobject($this->dbReservoir);
    $this->assertEquals($test->addYear("Cle Elum", $testYear), $expected);
	}
	public function testAddYear2(){
    $expected = "Invalid year format!";
    $testYear = array(
      "January" => 1,
      "February" => 1,
      "March" => 1,
      "April" => 1,
      "May" => 1,
      "June" => 1,
      "July" => 1,
      "August" => 1,
      "September" => 1,
      "October" => 1,
      "November" => 1,
    );
    $test = new Simulationobject($this->dbReservoir);
    $this->assertEquals($test->addYear("Cle Elum", $testYear), $expected);
	}
	public function testAddYear3(){
    $testYear = array(
      "January" => 1,
      "February" => 1,
      "March" => 1,
      "April" => 1,
      "May" => 1,
      "June" => 1,
      "July" => 1,
      "August" => 1,
      "September" => 1,
      "October" => 1,
      "November" => 1,
      "December" => 1,
    );
    $test = new Simulationobject($this->dbReservoir);
    $test->addYear("Cle Elum", $testYear);
    $this->assertEquals($test->getYear("Cle Elum", 1), $testYear);
	}
	public function testAddYear4(){
    $expected = "Invalid water level format";
    $testYear = array(
      "January" => "Poseidon",
      "February" => 1,
      "March" => 1,
      "April" => 1,
      "May" => 1,
      "June" => 1,
      "July" => 1,
      "August" => 1,
      "September" => 1,
      "October" => 1,
      "November" => 1,
      "December" => 1,
    );
    $test = new Simulationobject($this->dbReservoir);
    $this->assertEquals($test->addYear("Cle Elum", $testYear), $expected);
	}
	
	/* Tests for scaleYear
		1: Test for invalid reservoir name
		2: Test for invalid year index
		3: Test for correct scaling
		4: Test for invalid scalar
	*/
	public function testScaleYear(){
    $expected = "Invalid reservoir!";
    $testScalar = 1.1;
    $test = new Simulationobject($this->dbReservoir);
    $this->assertEquals($test->scaleYear("Poseidon", $testScalar, 0), $expected);
	}
	public function testScaleYear2(){
    $expected = "Invalid year index!";
    $testScalar = 1.1;
    $test = new Simulationobject($this->dbReservoir);
    $this->assertEquals($test->scaleYear("Cle Elum", $testScalar, 13), $expected);
	}
	public function testScaleYear3(){
    $testScalar = 1.1;
    $expected = array(
      "January" => 1*$testScalar,
      "February" => 1*$testScalar,
      "March" => 1*$testScalar,
      "April" => 1*$testScalar,
      "May" => 1*$testScalar,
      "June" => 1*$testScalar,
      "July" => 1*$testScalar,
      "August" => 1*$testScalar,
      "September" => 1*$testScalar,
      "October" => 1*$testScalar,
      "November" => 1*$testScalar,
      "December" => 1*$testScalar,
    );
    $testYear = array(
      "January" => 1,
      "February" => 1,
      "March" => 1,
      "April" => 1,
      "May" => 1,
      "June" => 1,
      "July" => 1,
      "August" => 1,
      "September" => 1,
      "October" => 1,
      "November" => 1,
      "December" => 1,
    );
    $test = new Simulationobject($this->dbReservoir);
    $test->addYear("Cle Elum", $testYear);
    $test->scaleYear("Cle Elum", $testScalar, 1);
    $this->assertEquals($test->getYear("Cle Elum", 1), $expected);
	}
	
	public function testScaleYear4(){
    $expected = "Invalid scalar!";
    $testScalar = "Poseidon";
    $test = new Simulationobject($this->dbReservoir);
    $this->assertEquals($test->scaleYear("Cle Elum", $testScalar, 0), $expected);
	}
	
	/* Tests for getSim
		1: Test that it works
	*/
	public function testGetSim(){
    $expected = $this->dbReservoir;
    $test = new Simulationobject($this->dbReservoir);
    $this->assertEquals($test->getSim(), $expected);
	}
}

?>