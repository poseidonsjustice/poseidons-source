<?php

include 'simulationObject.php';

class SimulationScenario {

  private $scalarA;
  private $scalarB;
  private $years;
  private $dbReservoir = array();
  private $sim;
  
   /*
    Class Constructor
    $type: Type of scenario being simulated
    $scalarA: First scalar variable to modify simulation
    $scalarB: Second scalar variable to modify simulation
    $years: Number of years to simulate
    $waterLevel: User selected percentage of current water levels
   */
  function __construct($type, $scalarA, $scalarB, $years, $waterLevel) {
    $this->populateReservoir($type, $waterLevel);
    $this->scalarA = $scalarA;
    $this->scalarB = $scalarB;
    $this->years = $years;
    $this->sim = new SimulationObject($this->dbReservoir);
    $this->calculate($type);
  }
  
   /*
    This function populats the dbReservoir
    $typeSeason: The appropriate base water levels for the scenario
    $waterLevel: The user selected percentage of the current base water levels
   */
  function populateReservoir($typeSeason, $waterLevel) {
    global $location_query;
    include 'connect.php';

    while ($loc_row = mysql_fetch_assoc($location_query)) {
      $resevoir_query = mysql_query("
      SELECT ml.month_name, wl.$typeSeason
      FROM initial_water_level AS wl
      LEFT JOIN month_list AS ml
      ON wl.month_id = ml.month_id
      WHERE wl.location_id = " . $loc_row['location_id'] . "
      ORDER BY wl.location_id, wl.month_id
    ");

      while ($res_row = mysql_fetch_array($resevoir_query)) {
        $tempArray[$res_row['month_name']] = $res_row[$typeSeason] * ($waterLevel / 100);
      }

      $this->dbReservoir[$loc_row['location_name']] = array(0 => $tempArray);
    }
    

  }
  
   /*
    This function calls the appropriate functions to calculate 
    data depending on selected scenario
    $sim: The simulation object
    $type: Type of scenario being simulated
   */
  function calculate($type) {

    // For each of the reservoirs in the simulation we need 
    // to calculate the first year then the other years
    foreach ($this->sim->getSim() as $reservoir => $i) {

      switch ($type) {
        case "wet_season": // Wet simulation
        case "dry_season": // Dry simulation
          // Calculate first year and then add it to sim object
          $firstYear = $this->calculateMayOct($reservoir, 0);
          $this->sim->addYear($reservoir, $firstYear);
          $copyYear = $this->sim->getYear($reservoir, 1);
          
          if ($this->years > 0) {
            $previousYear = $copyYear;
            // Add other years if we set them
            for ($i = 2; $i <= $this->years; $i++) {
              //$nextYear = $this->sim->addYear($reservoir, $previousYear);
              $this->sim->addYear($reservoir, $previousYear);
              $this->sim->scaleYear($reservoir, $this->scalarB, $i);
              $previousYear = $this->sim->getYear($reservoir, $i);
            }
          }
          break;
        case "normal_season": // Random/Predicted simulations
          for ($i = 0; $i <= $this->years; $i++) {
            $nextYear = $this->calculateOffset($reservoir, $i);
            $this->sim->addYear($reservoir, $nextYear);
          }
          break;
      }
    }  
  }
  // Displays the results for the calculations
  function displayResults(){
    $this->sim->printSimulation($this->years);
  }
  /*
    This function calculates the months of May to October for Wet/Dry simulations
    $sim: The simulation object
    $currentYear: Current year of the simulation containing the water levels for each month
    $predictedYear: Predicted year object
   */
  function calculateMayOct($reservoir, $currentYear) {
    $predictedYear;
    try{
      if(gettype($this->sim->getMonth($reservoir, $currentYear, "May")) === 'string')
        throw new Exception($this->sim->getMonth($reservoir, $currentYear, "May"));
      for ($x = 4; $x <= 9; $x++) {
        $predictedYear[$this->sim->getMonthName($x)] = $this->sim->getMonth($reservoir, $currentYear, $this->sim->getMonthName($x)) * $this->scalarA;
      }
      return $this->calculateJanApr($reservoir, $predictedYear, $currentYear);
    }
		catch(Exception $e){
			return $e->getMessage();
		}
  }

  /*
    This function calculates the months of January to April for Wet/Dry simulations
    $sim: The simulation object
    $year: The incomplete year object after simulating May to October
    $currentYear: Current year of the simulation containing the water levels for each month
    $predictedYear: Predicted year object
   */
  private function calculateJanApr($reservoir, $year, $currentYear) {
    $predictedYear = $year;
    try{
      if(gettype($this->sim->getMonth($reservoir, $currentYear, "January")) === 'string')
        throw new Exception($this->sim->getMonth($reservoir, $currentYear, "January"));
      for ($x = 0; $x <= 11; $x++) {
        if ($x <= 3 || $x >= 10) {
          $predictedYear[$this->sim->getMonthName($x)] = $this->sim->getMonth($reservoir, $currentYear, $this->sim->getMonthName($x)) * $this->scalarB;
        }
      }
      return $predictedYear;
    }
    catch(Exception $e){
			return $e->getMessage();
		}
  }

  /*
    This function calculates possible offset for the predicted/random values by altering
    them randomly through a range of -0.03 to 0.03 or -0.10 to 0.10 respectively
    $sim: The simulation object
    $currentYear: Current year of the simulation containing the water levels for each month
    $offset: Product of $predictedYear and $randomValue
    $predictedYear: Predicted year object
   */
  function calculateOffset($reservoir, $currentYear) {
    $randValue = mt_rand($this->scalarA, $this->scalarB) / 100;
    $predictedYear;
    try{
      if(gettype($this->sim->getMonth($reservoir, $currentYear, "January")) === 'string')
        throw new Exception($this->sim->getMonth($reservoir, $currentYear, "January"));
      for ($x = 0; $x <= 11; $x++) {
        $predictedYear[$this->sim->getMonthName($x)] = $this->sim->getMonth($reservoir, $currentYear, $this->sim->getMonthName($x));
        $offset = $predictedYear[$this->sim->getMonthName($x)] * $randValue;
        $predictedYear[$this->sim->getMonthName($x)] += $offset;
      }
    return $predictedYear;
    }
    catch(Exception $e){
			return $e->getMessage();
		}
  }
}
?>