<?php

include 'SimulationScenario.php';

/* Checks that form has been submitted with all values
 * Runs simulation with given values if true
 */
if (isset($_POST['climate'], $_POST['year'], $_POST['slider'])) {
  $years = $_POST['year'];
  $waterLevel = $_POST['slider'];
  $index = $_POST['climate'];
  
  simulate($years, $waterLevel, $index);
}

/* Calls the given simulation
 * index: Index of the simulation array [Dry, Wet, Predicted, Random]
 * waterLevel: Selected percentage for water level [20% - 200%]
 * years: Number of years in time scenario [1, 2, 3, 4, 5, 10]
 */
function simulate($years, $waterLevel, $index) {
  switch ($index) {
    case "wet": 
      $scene = new SimulationScenario("wet_season", 1.12, 1.08, $years, $waterLevel);
      $scene->displayResults();
      break;
    case "predicted":
      $scene = new SimulationScenario("normal_season", -3, 3, $years, $waterLevel);
      $scene->displayResults();
      break;
    case "random":
      $scene = new SimulationScenario("normal_season", -10, 10, $years, $waterLevel);
      $scene->displayResults();
      break;
    case "dry":
      $scene = new SimulationScenario("dry_season", .88, .92, $years, $waterLevel);
      $scene->displayResults();
      break;
  }
}
?>