<?php
include('SimulationScenario.php');

class simulationScenarioTest extends \PHPUnit_Framework_TestCase{
  
  /* Tests the calculateMayOct function
  1: Test for invalid reservoir name
  2: Test for invalid year
  3: Test for correct year and reservoir
  */
  function testCalculateMayOct(){
    $expected = "Invalid reservoir!";
    $test = new SimulationScenario("dry_season", .88, .92, 1, 100);
    $this->assertEquals($test->calculateMayOct("Poseidon", 0), $expected);
  }
  function testCalculateMayOct2(){
    $expected = "Invalid year index!";
    $test = new SimulationScenario("dry_season", .88, .92, 1, 100);
    $this->assertEquals($test->calculateMayOct("Cle Elum", 13), $expected);
  }
  function testCalculateMayOct3(){
    $expected = array(
      "January" => 33166*.92, 
      "February" => 89273*.92,
      "March" => 165172*.92,
      "April" => 220979*.92,
      "May" => 279532*.88,
      "June" => 342205*.88, 
      "July" => 301917*.88,
      "August" => 181014*.88,
      "September" => 95364*.88,
      "October" => 23380*.88,
      "November" => 11812*.92,
      "December" => 37445*.92);
    $test = new SimulationScenario("dry_season", .88, .92, 1, 100);
    $this->assertEquals($test->calculateMayOct("Cle Elum", 0), $expected);
  }
  
  /* Tests the calculateOffset function
  1: Test for invalid reservoir name
  2: Test for invalid year
  */
  function testCalculateOffset(){
    $expected = "Invalid reservoir!";
    $test = new SimulationScenario("normal_season", .88, .92, 1, 100);
    $this->assertEquals($test->calculateOffset("Poseidon", 0), $expected);
  }
  function testCalculateOffset2(){
    $expected = "Invalid year index!";
    $test = new SimulationScenario("normal_season", .88, .92, 1, 100);
    $this->assertEquals($test->calculateOffset("Cle Elum", 13), $expected);
  }
}

// TODO: Make automation to test other methods as they are display dependent and random/predicted
// have randomized scalars.
?>