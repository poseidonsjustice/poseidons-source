<?php

//object simYear containing  months
class SimulationObject{
  protected $months = array("January", "February", "March", "April", "May", "June", "July", "August",
      "September", "October", "November", "December");
  protected $years = array(
  0 => array(
    "January" => 1,
    "February" => 1,
    "March" => 1,
    "April" => 1,
    "May" => 1,
    "June" => 1,
    "July" => 1,
    "August" => 1,
    "September" => 1,
    "October" => 1,
    "November" => 1,
    "December" => 1, 
    )
  );
  protected $reservoirs = array();
  
  /*
  Constructor that sets the base year of this simulation
  $base: The year array sent
  */  
  function __construct($base){  
    while(list($key, $val) = each($base)){
      $this->setYears($base[$key]);
      $this->reservoirs[$key] = $val;
    }
  }
  
  /*
  Function that sets the base year of this simulation
  $base: The year array sent
  */
  private function setYears($base){
    $this->years = $base;
  }
  
  /*
  Function that adds a predicted year to the end of the array
  $predictedYear: The predicted year array
  Return: returns how many years are in given reservoir
  */
  public function addYear($reservoirName, $predictedYear){
		try{
			if(sizeof($predictedYear) != 12)
				throw new Exception("Invalid year format!");
			else{      
				foreach($predictedYear as $month)      
          if(!(gettype((int)$month) === 'integer') || !(gettype((double)$month) === 'double'))
            throw new Exception("Invalid water level format");
          else if((gettype($month) === 'string'))
            throw new Exception("Invalid water level format");
				$this->reservoirs[$reservoirName][sizeof($this->reservoirs[$reservoirName])] = $predictedYear;
				return sizeof($this->reservoirs[$reservoirName]);
			}
		}
		catch(Exception $e){
			return $e->getMessage();
		}
  }
  
  /*
  Function that returns this simulation object
  */
  public function getSim(){
    return $this->reservoirs;
  }
  
  /*
  Function that gets the water level for a given month name
  $year: The year to get the month of
  $monthName: The name of the month
  */
  public function getMonth($reservoir, $year, $monthName){
    try{
			if(array_key_exists($reservoir, $this->reservoirs)){
				if(array_key_exists($year, $this->reservoirs[$reservoir])){
					if(array_key_exists($monthName, $this->reservoirs[$reservoir][$year])){
						return $this->reservoirs[$reservoir][$year][$monthName];
					}
					else throw new Exception("Invalid month name!");
				}
				else throw new Exception("Invalid year index!");
			}
			else throw new Exception("Invalid reservoir!");
    }
    catch(Exception $e){
       return $e->getMessage();
    }
  }
  
  /*
  Function that gets a given year in this simulation
  $index: The index number in the years array
  */
  public function getYear($reservoir, $index){
    try{
			if(array_key_exists($reservoir, $this->reservoirs)){
				if(array_key_exists($index, $this->reservoirs[$reservoir]))
					return $this->reservoirs[$reservoir][$index];
				else throw new Exception("Invalid year index!");
			}
			else throw new Exception("Invalid reservoir!");
		}
    catch(Exception $e){
       return $e->getMessage();
    }
  }
  
  /*
  Function that gets the name of the months in the month array
  $index: The index in the months array
  */
  public function getMonthName($index){
		try{
			if(array_key_exists($index, $this->months))
				return $this->months[$index];
			else throw new Exception("Invalid month index.");
		}
		catch(Exception $e){
			return $e->getMessage();
		}
  }
  
  /*
  Function that scales the value for each month of the year
  $scalar: Value to scale months by
  $year: Index of the years array
  */
  public function scaleYear($reservoir, $scalar, $year){ 
    try{
			if(!array_key_exists($reservoir, $this->reservoirs))
				throw new Exception("Invalid reservoir!");
			else if(!array_key_exists($year, $this->reservoirs[$reservoir]))
				throw new Exception("Invalid year index!");
			else if(!(gettype($scalar) === 'double'))
				throw new Exception("Invalid scalar!");
			for ($x = 0; $x <= sizeof($this->months)-1; $x++) {
				$this->reservoirs[$reservoir][$year][$this->getMonthName($x)] = $this->getMonth($reservoir, $year, $this->getMonthName($x)) * $scalar;
			}
		}
		catch(Exception $e){
			return $e->getMessage();
		}
  }
  
  public function printSimulation($years){
	  
	foreach($this->getSim() as $reservoir => $j){

	  echo '<center><table border="1"><caption>'.$reservoir.'</caption><tr><td></td>';
	  for($i=0; $i<=$years; $i++){
        if ($i == 0) {
          $y = "Base Year";
        } else {
          $y = $i;
        }
        echo '<td><center>'.$y.'</center></td>';
      }
	  echo '</tr>';
	  for($m = 0; $m <= 11; $m++){
        echo '<tr><td><center>'. $this->getMonthName($m). '</center></td>';
		  for($y = 0; $y <= $years; $y++){
		    $yearValue = $this->getYear($reservoir, $y)[$this->getMonthName($m)];
		    //echo $this->getMonthName($m)." in year " .$this->getYear($y)[$this->getMonthName($m)]. "<br/>";
		    echo '<td><center>'.number_format("$yearValue",0).'</center></td>';
	      }
	  
        echo '</tr>';
	  }

	  echo '</table>*All units are in acre-feet (ac-ft)</center><br>';
    }
  }
}

?>